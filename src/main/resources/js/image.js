angular.module('faceDetection',[])
	.directive('ngFile',['$parse',function($parse){
		return {
			link: function(scope,element,attrs){
				var onChange = $parse(attrs.ngFile);
				element.on('change',function(event){
					onChange(scope,{$file:event.target.files});
				});
			}
		}
	}])
	.controller('processImg',['$scope','$http',function($scope,$http){
		var formData ;
		$scope.getFile = function($file){
			var reader = new FileReader();
			reader.onload = $scope.loadIMG;
			reader.readAsDataURL($file[0]);
			formData = new FormData();
			formData.append('imgP',$file[0]);
			$scope.nameFile = $file[0].name;
		}
		$scope.loadIMG = function(e){
			$scope.$apply(function(){
				$scope.preview = e.target.result;
				$scope.imgRes ='';
				$("#imgResponse").attr('src','');
			})
		}
		var spinner = null;
		var spinner_div = 0;
		$scope.uploadFile = function(){
		var name_file = $('#nameF').text();
		if(name_file == ""){
			alert('Bạn chưa chọn ảnh');
			}else{
			$(".overBG").css('display','block');
			 var opts = {
				      lines: 12, // The number of lines to draw
				      length: 20, // The length of each line
				      width: 10, // The line thickness
				      radius: 30, // The radius of the inner circle
				      corners: 1, // Corner roundness (0..1)
				      rotate: 0, // The rotation offset
				      direction: 1, // 1: clockwise, -1: counterclockwise
				      color: '#000', // #rgb or #rrggbb
				      speed: 0.8, // Rounds per second
				      trail: 60, // Afterglow percentage
				      shadow: false, // Whether to render a shadow
				      hwaccel: false, // Whether to use hardware acceleration
				      className: 'spinner', // The CSS class to assign to the spinner
				      zIndex: 2e9, // The z-index (defaults to 2000000000)
				      top: 'auto', // Top position relative to parent in px
				      left: 'auto' // Left position relative to parent in px
				    };
			 		spinner_div = $('#spinner').get(0);
			 		if(spinner == null) {
			 	          spinner = new Spinner(opts).spin(spinner_div);
			 	        } else {
			 	          spinner.spin(spinner_div);
			 	        }
			/*Send data to server*/
			$http({
				url :'imageProcess',
				method : 'POST',
				data:formData,
				headers:{'Content-Type':undefined}
			}).then(function(response){
				//$(".showIMG").html("<img src='"+response.data+"' width='100%' height='300px' class='img-responsive' />");
				var obj = response.data.linkIMG;
				if(obj[0] =="NoResults"){
					$("#upload").blur();
					alert("Không có kết quả trả về. Vui lòng thử lại");
				} else{
					$("#upload").blur();
					$scope.imgRes = obj[0];
				}	
				spinner.stop(spinner_div);
				$("#overBG").css('display','none');
				console.log("Process image successfull "+obj[0]);
			},function(errRes){
				$("#upload").blur();
				//if(errRes.data.status === 503){
				spinner.stop(spinner_div);
				$("#overBG").css('display','none');
				alert('Server bị gián đoạn. Vui lòng bấm (F5) và thực hiện lại');
				//}
				console.log("Error process image "+errRes.data);
			});
		}
		}
		
	}]);